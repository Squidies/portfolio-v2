# Nuxt Starter Template

## Install

```bash
# npm
npm install

# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# yarn
yarn dev
```

### Dependencies

- [Nuxt 3](https://nuxt.com/docs/)
  - @nuxt modules
  - [@nuxt/content](https://content.nuxtjs.org/)
  - [@nuxt/google-fonts](https://google-fonts.nuxtjs.org/)
  - [@nuxt/tailwindcss](https://tailwindcss.nuxtjs.org/)
  - [@nuxt/color-mode](https://color-mode.nuxtjs.org/)
- [Prettier](https://prettier.io/)
  - Prettier-plugins
  - [prettier-plugin-tailwind](https://github.com/tailwindlabs/prettier-plugin-tailwindcss)
- [FontAwesome](https://fontawesome.com/)
