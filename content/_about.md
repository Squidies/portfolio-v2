---
title: About
head.title: Kirtis McGlynn | Frontend JavaScript Developer
slug: about
---

::ContentAbout{image="assets/images/squidies-portrait.jpg" title="Kirtis portrait"}

Hi! I'm Kirtis McGlynn, a Frontend Javascript Developer who specializes in crafting awesome web interfaces that are responsive, accessible, and user-friendly.

I've been obsessed with computers and how they work for as long as I can remember. In fact, I learned HTML and CSS ages ago, way before I even considered making it my career!

When I'm not coding, you can find me jamming on my guitar, tinkering with my [3D printers](https://squidies3dprintys.etsy.com/){ title="Squidies3DPrintys Etsy Shop" target="\_blank" .link-alt }, or observing my pet tarantulas to study their habits and be a better web developer.
::
