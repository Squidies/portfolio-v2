---
title: Web Application Developer
company: Mediacom Communications
dates: { start: "February 2016", end: "November 2019" }
---

- Collaborated with stakeholders to design and develop intuitive user experiences for the Mediacom Provisioning Suite, significantly reducing response times for detecting and resolving customers service and equipment related issues.

- Developed the Mediacom Community Wi-Fi Hotspot Finder App, which boosted user engagement with public Wi-Fi networks by 20%, increasing connectivity and overall user satisfaction in target cities.

- Developed custom UI design framework for [mediacomcable.com](https://mediacomcable.com/){ title="Link to Mediacom Cable" target="\_blank" .link-alt }, reducing development time for new promotional and informational materials.
