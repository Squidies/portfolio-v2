---
title: Project Manager & Web Development
company: Freelance
dates: { start: "July 2015", end: "January 2016" }
---

- Led the development of [theeriehotel.com](https://theeriehotel.com/){ target="\_blank" .link-alt }, delivering a highly responsive website that significantly enhanced their online presence.
