export default function throttle(
  func,
  delay,
  options = { leading: true, trailing: true },
) {
  let timeoutId;
  let lastExecTime = 0;

  return function (...args) {
    const currentTime = new Date().getTime();

    if (!lastExecTime && !options.leading) {
      lastExecTime = currentTime;
    }

    const timeSinceLastExec = currentTime - lastExecTime;

    if (timeSinceLastExec >= delay) {
      if (timeoutId) {
        clearTimeout(timeoutId);
        timeoutId = null;
      }
      func.apply(this, args);
      lastExecTime = currentTime;
    } else if (!timeoutId && options.trailing) {
      timeoutId = setTimeout(() => {
        func.apply(this, args);
        lastExecTime = options.leading ? new Date().getTime() : 0;
        timeoutId = null;
      }, delay - timeSinceLastExec);
    }
  };
}
